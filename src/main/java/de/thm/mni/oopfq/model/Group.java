package de.thm.mni.oopfq.model;

import java.util.Set;

public class Group {
    private Professor professor;
    private Set<Student> student;
    private int gid;

    public Group(Professor professor, Set<Student> student) {
        this.professor = professor;
        this.student = student;
        this.gid = (int) (Math.random()*100) + 1 ;
    }

    public Professor getProfessor() {
        return professor;
    }

    public Set<Student> getStudent() {
        return student;
    }

    public int getGid() {
        return gid;
    }
}

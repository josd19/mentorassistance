package de.thm.mni.oopfq.model;

public class Person {
    private String fname;
    private String sname;
    private String id;


    public Person(String fname, String sname, String id) {
        this.fname = fname;
        this.sname = sname;
        this.id = id;
    }




    //Get Methods
    public String getSname() {
        return sname;
    }

    public String getFname() {
        return fname;
    }

    public String getID() {
        return id;
    }
}


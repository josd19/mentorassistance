package de.thm.mni.oopfq.model;

import java.util.Set;

public class Professor extends Person {
    private String office;
    private Set <String> compentencies;

    public Professor(String fname, String sname, String id, String office, Set<String> compentencies) {
        super(fname, sname, id);
        this.office = office;
        this.compentencies = compentencies;
    }


    //Set Methods...

    public void setCompentencies(Set<String> compentencies) {
        this.compentencies = compentencies;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    //Get Methods
    public String getOffice() {
        return office;
    }

    public Set<String> getCompentencies() {
        return compentencies;
    }
}

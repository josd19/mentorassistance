package de.thm.mni.oopfq.model;

import java.util.Set;

public class Student extends Person {
    private Set<String> interests;

    public Student(String fname, String sname, String id, Set<String> interests) {
        super(fname, sname, id);
        this.interests = interests;
    }

    //Set Methods...

    public void setInterests(Set<String> interests) {
        this.interests = interests;
    }


    //Get Methods

    public Set<String> getInterests() {
        return interests;
    }
}
